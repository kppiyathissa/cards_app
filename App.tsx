import React from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Provider} from 'react-redux';

import GameContainer from './src/containers/game-container';
import store from './src/store';

export default () => (
  <Provider store={store}>
    <SafeAreaProvider>
      <GameContainer />
    </SafeAreaProvider>
  </Provider>
);

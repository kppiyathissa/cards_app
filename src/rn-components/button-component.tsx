/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import {responsiveUnits} from '../lib/func';

import colors from '../res/theme/colors';

type IProps = {
  text?: string;
  onPress?: () => void;
  testID?: string;
};

const Button = ({text, onPress, testID}: IProps) => (
  <TouchableOpacity
    style={{
      justifyContent: 'center',
      alignItems: 'center',
      paddingVertical: 10,
    }}
    testID={testID}
    onPress={onPress}>
    <Text
      style={{
        color: colors.primaryColor,
        fontSize: responsiveUnits(22),
      }}>
      {text}
    </Text>
  </TouchableOpacity>
);

export default Button;

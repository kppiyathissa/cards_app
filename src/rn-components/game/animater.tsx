/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useRef} from 'react';
import {
  Animated,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  StyleProp,
  ViewStyle,
} from 'react-native';
import {batch, useDispatch, useSelector} from 'react-redux';

import {
  addToMatchIds,
  incrementTotalClicks,
  setSelectedItem,
} from '../../store/actions/card-actions';

import {Reducers} from '../../lib/types';

import Card from '../card-component';
import styles from './styles';

const FlipCard = ({text, id, containerStyle}: IProps) => {
  const dispatch = useDispatch();

  const cardOpenStatus = useRef<CardOpenStaus>(CardOpenStaus.CLOSED);
  const animation = useRef(new Animated.Value(0));
  const rotation = animation.current.interpolate({
    inputRange: [0, 1],
    outputRange: ['0deg', '180deg'],
    extrapolate: 'clamp',
  });

  const backCardOpacity = animation.current.interpolate({
    inputRange: [0, 0.5, 1],
    outputRange: [0, 0, 1],
    extrapolate: 'clamp',
  });

  const frontCardOpacity = animation.current.interpolate({
    inputRange: [0, 0.5, 1],
    outputRange: [1, 1, 0],
    extrapolate: 'clamp',
  });

  const scale = animation.current.interpolate({
    inputRange: [0, 0.5, 1],
    outputRange: [1, 1.3, 1],
  });
  const totalCount = useSelector(
    (state: Reducers) => state.cardReducer.totalClicks,
  );
  const {prevItem, currentItem} = useSelector((state: Reducers) => ({
    prevItem: state.cardReducer.previousItem,
    currentItem: state.cardReducer.currentItem,
  }));

  const closeCard = () =>
    Animated.timing(animation.current, {
      toValue: 0,
      useNativeDriver: true,
      duration: 500,
    });

  useEffect(() => {
    // close the card when user reset the app
    if (totalCount === 0 && cardOpenStatus.current === CardOpenStaus.OPENED) {
      closeCard().start(() => (cardOpenStatus.current = CardOpenStaus.CLOSED));
      return;
    }
    if (totalCount !== 0 && totalCount % 2 === 0) {
      const hasId = id === prevItem.id || currentItem.id === id;
      if (currentItem.value === prevItem.value && hasId) {
        dispatch(addToMatchIds({matchedIds: id}));
        return;
      } else if (hasId) {
        Animated.sequence([Animated.delay(1000), closeCard()]).start(() => {
          cardOpenStatus.current = CardOpenStaus.CLOSED;
        });
      }
    }
  }, [totalCount]);

  const onPress = () => {
    if (cardOpenStatus.current === CardOpenStaus.OPENED) {
      return;
    }
    cardOpenStatus.current = CardOpenStaus.OPENED;
    batch(() => {
      dispatch(incrementTotalClicks());
      dispatch(setSelectedItem({item: {id, value: text as number}}));
    });
    Animated.timing(animation.current, {
      toValue: 1,
      useNativeDriver: true,
      duration: 500,
    }).start();
  };

  return (
    <TouchableWithoutFeedback testID={id?.toString()} onPress={onPress}>
      <Animated.View
        style={{
          transform: [{rotateY: rotation}, {scale}],
        }}>
        <Card
          containerStyles={[
            styles.cardStyle,
            styles.cardBorder,
            containerStyle,
          ]}>
          <Animated.View
            style={[
              StyleSheet.absoluteFill,
              styles.textContainer,
              {
                opacity: backCardOpacity,
              },
            ]}>
            <Text style={styles.text}>{text}</Text>
          </Animated.View>
          <Animated.View
            style={[
              styles.innerCard,
              styles.cardBorder,
              {opacity: frontCardOpacity},
            ]}>
            <Text style={[styles.text, styles.whiteText]}>?</Text>
          </Animated.View>
        </Card>
      </Animated.View>
    </TouchableWithoutFeedback>
  );
};

type IProps = {
  text: string | number;
  id: number;
  containerStyle?: StyleProp<ViewStyle>;
};

export enum CardOpenStaus {
  OPENED = 'OPENDED',
  CLOSED = 'CLOSED',
}

export default FlipCard;

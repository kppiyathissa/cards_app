import {StyleSheet} from 'react-native';

import {responsiveUnits} from '../../lib/func';
import colors from '../../res/theme/colors';

const styles = StyleSheet.create({
  cardStyle: {
    padding: responsiveUnits(5),
    flexDirection: 'row',
  },
  innerCard: {
    backgroundColor: colors.primaryColor,
    height: '100%',
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  cardBorder: {
    borderRadius: 10,
  },
  textContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
    transform: [{scaleX: -1}],
  },
  text: {
    fontWeight: '600',
    fontSize: responsiveUnits(24),
    color: colors.blackColor1,
  },
  whiteText: {
    color: colors.white,
  },
  container: {
    backgroundColor: 'rgba(0,0,0,0.2)',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontWeight: '500',
    fontSize: responsiveUnits(22),
    marginBottom: 5,
  },
  alertBox: {
    backgroundColor: colors.white,
    borderRadius: 10,
  },
  header: {
    paddingVertical: responsiveUnits(20),
    alignItems: 'center',
    paddingHorizontal: responsiveUnits(50),
  },
  footer: {
    paddingVertical: responsiveUnits(5),
    borderTopColor: colors.lightgray,
    borderTopWidth: 1,
  },
  subtitle: {
    fontSize: responsiveUnits(11),
  },
});

export default styles;

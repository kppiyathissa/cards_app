import React from 'react';
import {fireEvent} from '@testing-library/react-native';

import FlipCard from '../game/animater';
import {storeRenderer, reduxRenderer} from '../../lib/test-funcs';
import store from '../../store';
import {Reducers} from '../../lib/types';

describe('FlipCard', () => {
  it('On load Back card should be visible', () => {
    const C = <FlipCard text={50} id={2} />;
    const {getByText} = storeRenderer(C, mockStore());
    expect(getByText('?')).toBeTruthy();
  });

  it('FrontView should be visible when card pressed', () => {
    const C = <FlipCard text={43} id={3} />;
    const {getByTestId} = reduxRenderer(C);
    const button = getByTestId('3');
    expect(button).toBeTruthy();
    fireEvent.press(button);
    expect((store.getState() as Reducers).cardReducer.totalClicks).toEqual(1);
    expect(
      (store.getState() as Reducers).cardReducer.currentItem.value,
    ).toEqual(43);
  });
});

const mockStore = (totalClicks = 0) => ({
  cardReducer: {
    totalClicks,
    previousItem: {id: 5, value: 30},
    currentItem: {id: 5, value: 30},
  },
});

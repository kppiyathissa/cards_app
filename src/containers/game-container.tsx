/* eslint-disable react-hooks/exhaustive-deps */
import React, {useEffect, useMemo, useState} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  ScrollView,
  LayoutChangeEvent,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

import {resetCards, setCards} from '../store/actions/card-actions';
import {Reducers} from '../lib/types';
import {generateCards, responsiveUnits} from '../lib/func';

import FlipCard from '../rn-components/game/animater';
import Congrats from '../rn-components/game/animated-greeting';
import Button from '../rn-components/button-component';

import styles from './game-style';
import useDimension from '../lib/hooks/matrices';

const MARGIN = responsiveUnits(20);

const GameScreen = () => {
  const dispatch = useDispatch();

  const {bottom, top} = useSafeAreaInsets();
  const {HEIGHT, WIDTH} = useDimension();
  const [offset, setOffset] = useState(0);
  const cards = useSelector((state: Reducers) => state.cardReducer.cards);
  const totalSteps = useSelector(
    (state: Reducers) => state.cardReducer.totalClicks,
  );
  const matchedIds = useSelector(
    (state: Reducers) => state.cardReducer.matchedIds,
  );
  const success = useMemo(
    () => matchedIds.length > 0 && matchedIds.length === cards.length,
    [matchedIds],
  );

  const loadCards = () => {
    const cardsArray = generateCards(6).map((el, index) => ({
      id: index,
      value: el,
    }));
    dispatch(
      setCards({
        cards: cardsArray,
      }),
    );
  };

  const onChnageLayout = (event: LayoutChangeEvent) => {
    setOffset(event.nativeEvent.layout.height + top + bottom + MARGIN * 4);
  };

  const renderHeader = () => (
    <View style={styles.header} onLayout={onChnageLayout}>
      <Button onPress={restart} text="Restart" />
      <View testID="STEPS" style={styles.text}>
        <Text style={styles.countDescriptionText}>STEPS: </Text>
        <Text style={styles.countText}>{totalSteps}</Text>
      </View>
    </View>
  );

  const restart = () => {
    dispatch(resetCards());
    setTimeout(() => loadCards(), 500);
  };

  useEffect(() => {
    loadCards();
  }, []);

  return (
    <SafeAreaView style={[styles.container]}>
      <ScrollView>
        {renderHeader()}
        <View style={styles.cardContainer}>
          {cards.map((el, index) => (
            <FlipCard
              containerStyle={{
                width:
                  WIDTH > HEIGHT ? HEIGHT / 3 - MARGIN : WIDTH / 3 - MARGIN,
                height: (HEIGHT - offset) / 4,
                marginVertical: MARGIN / 2,
                marginHorizontal: MARGIN / 2,
              }}
              id={el.id}
              text={el.value}
              key={index.toString()}
            />
          ))}
        </View>
        {success && (
          <Congrats
            buttonText="Try another round"
            title="Congrats!"
            subtitle={`You win this game by ${totalSteps} steps!`}
            onPress={restart}
          />
        )}
      </ScrollView>
    </SafeAreaView>
  );
};

export default GameScreen;

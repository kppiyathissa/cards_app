import {cleanup} from '@testing-library/react-native';
import React from 'react';

import {storeRenderer} from '../../lib/test-funcs';
import {CardObjectType} from '../../lib/types';
import GameScreen from '../game-container';

jest.useFakeTimers();

describe('Main Screen', () => {
  afterEach(cleanup);
  it('Restart button and initial count 0 should be visible', () => {
    const {getByText, getByTestId} = storeRenderer(<GameScreen />, mockStore());
    expect(getByText('Restart')).toBeTruthy();
    expect(getByTestId('STEPS')).toBeTruthy();
  });

  it('given genarated cards should render a list of cards', () => {
    const {getAllByText} = storeRenderer(
      <GameScreen />,
      mockStore(
        0,
        [],
        [
          {id: 1, value: 1},
          {id: 2, value: 2},
          {id: 1, value: 1},
          {id: 2, value: 2},
          {id: 3, value: 3},
          {id: 3, value: 3},
        ],
      ),
    );
    expect(getAllByText('2')).toHaveLength(2);
    expect(getAllByText('3')).toHaveLength(2);
    expect(getAllByText('1')).toHaveLength(2);
  });

  it('If play finish should show greetings modal', () => {
    const {getByText} = storeRenderer(
      <GameScreen />,
      mockStore(
        0,
        [1, 1, 2, 2, 3, 3],
        [
          {id: 1, value: 1},
          {id: 2, value: 2},
          {id: 1, value: 1},
          {id: 2, value: 2},
          {id: 3, value: 3},
          {id: 3, value: 3},
        ],
      ),
    );
    expect(getByText('Congrats!')).toBeTruthy();
  });
});

const mockStore = (
  totalClicks = 0,
  matchedIds: Array<Number> = [],
  cards: Array<CardObjectType> = [],
) => ({
  cardReducer: {
    totalClicks,
    previousItem: {id: 1, value: 100},
    currentItem: {id: 2, value: 100},
    matchedIds,
    cards,
  },
});

import {StyleSheet} from 'react-native';
import {responsiveUnits} from '../lib/func';

import colors from '../res/theme/colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.blackColor1,
  },
  countDescriptionText: {
    color: colors.white,
    fontSize: responsiveUnits(24),
    fontWeight: '600',
  },
  countText: {
    fontSize: responsiveUnits(34),
    color: colors.primaryColor,
    textAlignVertical: 'bottom',
    fontWeight: '600',
  },
  text: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: responsiveUnits(10),
  },
  cardContainer: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
});

export default styles;

import {
  SET_MACTHINGS,
  INCREMENT,
  RESET_GAME,
  ADD_CARDS,
  SET_ITEM,
} from '../../constants/app-constants';
import {CardObjectType} from '../../lib/types';

export const setCards = (payload: {cards: Array<CardObjectType>}) => ({
  type: ADD_CARDS,
  payload,
});

export const incrementTotalClicks = () => ({
  type: INCREMENT,
});

export const setSelectedItem = (payload: {item: CardObjectType}) => ({
  type: SET_ITEM,
  payload,
});

export const addToMatchIds = (payload: {matchedIds: number}) => ({
  type: SET_MACTHINGS,
  payload,
});

export const resetCards = () => ({type: RESET_GAME});

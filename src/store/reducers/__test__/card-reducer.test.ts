import {incrementTotalClicks, setCards} from '../../actions/card-actions';
import {CardObjectType, CardReducerType} from '../../../lib/types';

import cardreducer from '../card-reducer';

const mockReducer = (
  totalClicks = 0,
  matchedIds: Array<number> = [],
  cards: Array<CardObjectType> = [],
): CardReducerType => ({
  totalClicks,
  matchedIds,
  cards,
  previousItem: {id: 2, value: 200},
  currentItem: {id: 1, value: 100},
});

describe('card reducer', () => {
  it('State should update with type ADD_CARDS Action', () => {
    const cards: Array<CardObjectType> = [
      {id: 1, value: 1},
      {id: 2, value: 2},
      {id: 3, value: 3},
      {id: 1, value: 1},
      {id: 2, value: 2},
      {id: 2, value: 2},
    ];
    expect(cardreducer(mockReducer(), setCards({cards}))).toEqual({
      ...mockReducer(),
      cards,
    });
  });
  it('State should update with type INCREMENT Action ', () => {
    expect(cardreducer(mockReducer(), incrementTotalClicks())).toEqual({
      ...mockReducer(),
      totalClicks: 1,
    });
  });
});

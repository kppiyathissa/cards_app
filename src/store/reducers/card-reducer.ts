import {
  SET_MACTHINGS,
  INCREMENT,
  RESET_GAME,
  ADD_CARDS,
  SET_ITEM,
} from '../../constants/app-constants';

import {CardReducerType} from '../../lib/types';

const INTIAL_STATE: CardReducerType = {
  previousItem: {id: 1, value: 100},
  currentItem: {id: 2, value: 200},
  matchedIds: [],
  totalClicks: 0,
  cards: [],
};

export default (
  state = INTIAL_STATE,
  {type, payload}: {type: string; payload?: any},
) => {
  switch (type) {
    case INCREMENT:
      return increment(state);
    case SET_ITEM:
      return setItem(state, {type, payload});
    case SET_MACTHINGS:
      return {
        ...state,
        matchedIds: [...state.matchedIds, payload?.matchedIds],
      };
    case ADD_CARDS:
      return {
        ...state,
        cards: payload?.cards,
      };
    case RESET_GAME:
      return {
        ...INTIAL_STATE,
        cards: state?.cards,
      };
    default:
      return state;
  }
};

const increment = (state: typeof INTIAL_STATE) => ({
  ...state,
  totalClicks: state.totalClicks + 1,
});

const setItem = (
  state: typeof INTIAL_STATE,
  {payload}: {type: string; payload?: any},
) => ({
  ...state,
  currentItem: payload?.item,
  previousItem: state.currentItem,
});

import React, {ReactElement} from 'react';
import {Provider} from 'react-redux';
import configureStore from 'redux-mock-store';
import {render} from '@testing-library/react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';

import store from '../store';

export const storeRenderer = (component: ReactElement, mockStore: object) => {
  const mockReducer = configureStore([]);

  return {
    ...render(
      <SafeAreaProvider
        initialSafeAreaInsets={{top: 0, left: 0, right: 0, bottom: 0}}>
        <Provider store={mockReducer(mockStore)}>{component}</Provider>
      </SafeAreaProvider>,
    ),
    store,
  };
};

export const reduxRenderer = (component: ReactElement) => {
  return {
    ...render(
      <SafeAreaProvider
        initialSafeAreaInsets={{top: 0, left: 0, right: 0, bottom: 0}}>
        <Provider store={store}>{component}</Provider>{' '}
      </SafeAreaProvider>,
    ),
    store,
  };
};

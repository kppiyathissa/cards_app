import {Dimensions} from 'react-native';

export const generateCards = (numberOfElements: number) => {
  // generating
  let array: Array<number> = [];
  while (array.length < numberOfElements) {
    const t = Math.floor(Math.random() * 100) + 1;
    if (array.find(el => el === t) === undefined) {
      array.push(t);
    }
  }
  // making pairs
  array = array.reduce<Array<number>>(
    (acc, currentvalue) => acc.concat([currentvalue, currentvalue]),
    [],
  );
  // shuffling
  array = array.sort(() => Math.random() - 0.5);
  return array;
};

export const {height: SCREEN_HEIGHT, width: SCREEN_WIDTH} =
  Dimensions.get('screen');

export const responsiveUnits = (size: number) => {
  //Air-Bnb standard resposive unit calculation for react-native
  const dimension = Math.min(SCREEN_HEIGHT, SCREEN_WIDTH);
  const dpi = Math.round(dimension / 375);
  if (dpi >= 2) {
    return (size * dpi) / 4 + size;
  }
  return size;
};
